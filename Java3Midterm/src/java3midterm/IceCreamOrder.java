
package java3midterm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

enum flavList {
        VANILLA, CHOCOLATE, STRAWBERRY, ROCKYROAD;
    }
public class IceCreamOrder {
    
    
    private String customerName; // 2-20 characters, regex to allow only uppercase, lowercase, digits, space, -,.()'"
    private Date deliveryDate; // 'y-m-d h:m' must be right now or later, and no further than 100 days ahead - get current date/time from new Date() and use Calendar
    // ArrayList<Flavour> flavList = new ArrayList<Flavour>; // duplicates allowed, but setter must verify that list must not be empty (at least 1 item in it)
    private flavList Flavour; // duplicates allowed, but setter must verify that list must not be empty (at least 1 item in it)
    
    // constructor
    public IceCreamOrder(String customerName, Date deliveryDate, flavList Flavour) throws DataInvalidException {
        setCustomerName(customerName);
        setDeliveryDate(deliveryDate);
        setFlavour(Flavour);
    }
    
    public IceCreamOrder(String dataLine) throws DataInvalidException {
        String[] data = dataLine.split(";");
        if (data.length < 3) {
            throw new DataInvalidException("Must have customer name, delivery date, and at least one flavour.");
        }
        try {
            String order = data[0];
            String customerName = data[1]; // ex IllegalArgumentException
            Date deliveryDate = dateFormatFile.parse(data[2]); // ex ParseException
            String flavDataLine = data[3]; // ex IllegalArgumentException
//            for (int i = 3; i<= data.length; i++){
//                flavList Flavour = flavList.valueOf(data[i]);
//            }               - tried to make it so it works for more than just one flavour but no luck

            String[] flavData = flavDataLine.split(",");
            for (int i = 0; i < flavData.length; i++){
                if (flavData[i] == flavList.values().toString()) {
                    break;
                } else {
                    throw new DataInvalidException("Invalid flavour selection, please choose ammong the list.");
                }
            }
            setCustomerName(customerName);
            setDeliveryDate(deliveryDate);
            
        } catch (ParseException ex) {
            throw new DataInvalidException("Date format invalid", ex);
        }
    }
    
    // getters and setters
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) throws DataInvalidException { // 2-20 characters, regex to allow only uppercase, lowercase, digits, space, -,.()'"
        if(customerName.matches("[a-zA-Z0-9.,()'-]{2,20}")){
            throw new DataInvalidException("Task must be 2-50 character long and not contain ;|` characters");
        }
        this.customerName = customerName;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) { // 'y-m-d h:m' must be right now or later, and no further than 100 days ahead - get current date/time from new Date() and use Calendar
        this.deliveryDate = deliveryDate;
    }

    public flavList getFlavour() {
        return Flavour;
    }

    public void setFlavour(flavList Flavour) { // duplicates allowed, but setter must verify that list must not be empty (at least 1 item in it)

        this.Flavour = Flavour;
    }
    
    private static final SimpleDateFormat dateFormatFile;

    static { // static initializer
        dateFormatFile = new SimpleDateFormat("yyyy-MM-dd 'at' hh:mm");
        dateFormatFile.setLenient(false);
    }
    
    @Override
    public String toString(){
        String deliveryDateStr = Globals.dateFormatScreen.format(deliveryDate);
        return String.format(" Customer: %s \n Delivery Date: %s \n Flavour(s): %s", customerName, deliveryDateStr,Flavour);
    }
    
    public String toDataString(){
        String deliveryDateStr = dateFormatFile.format(deliveryDate);
        return String.format("%s;%s;%s", customerName, deliveryDateStr,Flavour);
    }
    

}
