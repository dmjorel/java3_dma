
package java3midterm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Java3Midterm {

    static ArrayList<IceCreamOrder> ordersList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static ArrayList<String> flavItems = new ArrayList<>();
    
    static int inputInt() {
        while (true) {
            try {
                int result = input.nextInt(); // ex InputMismatchException
                input.nextLine(); // consume the leftover newline
                return result;
            } catch (InputMismatchException ex) {
                System.out.println("Invalid input, enter an integer. Try again.");
                input.nextLine(); // consume the invalid input
            }
        }
    }
    
    static int getOrderOption(){
        System.out.println("Available options: \n"
                + "1. Add an Order.\n"
                + "2. List Orders by Customer Name.\n"
                + "3. List Orders by Delivery Date.\n"
                + "0. Exit.\n"
                + "You selected: ");
        int option = inputInt();
        return option;
    }
    
    public static void main(String[] args) throws DataInvalidException {
        
        while (true){
            loadDataFromFile();
            int option = getOrderOption();
            switch (option){
                case 1:
                    addOrder();
                    break;
                case 2:
                    listOrdersName();
                    break;
                case 3:
                    listOrdersDeliveryDate();
                    break;
                case 0:
                    saveDataToFile();
                    System.out.println("Exiting.");
                    return;
                default:
                    System.out.println("Invalid choice, try again");
            }
        }
    }
    
    private static void addOrder() throws DataInvalidException  {
        try {
            System.out.println("Adding a new Order.");
            System.out.println("Enter customer name: ");
            String customerName = input.nextLine();
            System.out.println("Enter delivery date and time (yyyy/MM/dd at hh:mm): ");
            String deliDate = input.nextLine();
            Date deliveryDate = Globals.dateFormatScreen.parse(deliDate);
            System.out.println("Available flavours: ");
            for(flavList Flavour : flavList.values()){
                System.out.println(Flavour);
            }
            System.out.println("Enter which flavour to add, empty to finish: ");
            //String flavItem = input.nextLine()
//            while (flavItem != ""){
//                
//                flavItems.add(flavItem);       
//            }
            
            
            
            
            // I realized too late i needed to add the flavours being entered into an array 
//            if (flavItem != ""){
//                
//            } else {
//                
//            }
//            if(flav == flavList.values().toString()){
//                flavList Flavour = flavList.valueOf(flav); // ex IllegalArgumentException
//            } else {
//                throw new DataInvalidException("Invalid flavour selection, please choose ammong the list.");
//            }
            
            
        } catch (ParseException | NumberFormatException ex) {
            System.out.println("Error parsing: " + ex.getMessage());
        }
    }
    
    private static void listOrdersName(){
        ordersList.sort(new Comparator<IceCreamOrder>(){
            @Override
            public int compare(IceCreamOrder arg01, IceCreamOrder arg02){
                int comparison = 0;
                comparison = arg01.getCustomerName().compareTo(arg02.getCustomerName());
                return comparison;
            }
        });
    }
    
    private static void listOrdersDeliveryDate(){
            ordersList.sort(new Comparator<IceCreamOrder>(){
            @Override
            public int compare(IceCreamOrder arg01, IceCreamOrder arg02){
                int comparison = 0;
                comparison = arg01.getDeliveryDate().compareTo(arg02.getDeliveryDate());
                return comparison;
            }
        });
    }
    
    final static String DATA_FILE_NAME = "orders.txt";
    
    static void loadDataFromFile(){
        try(Scanner inputFile = new Scanner(new File(DATA_FILE_NAME))){
            while (inputFile.hasNextLine()){
                String line = "";
                try {
                    line = inputFile.nextLine();
                    ordersList.add(new IceCreamOrder(line)); // ex DataInvalid Exception
                } catch (DataInvalidException ex){
                    System.out.printf("Error parsing line (%s) ignoring: %s\n", ex.getMessage(), line);
                }
            }
        } catch (FileNotFoundException ex){
            System.out.println("File not found.");
        }
    }
    
    static void saveDataToFile() {
        try (PrintWriter outputFile = new PrintWriter(new File(DATA_FILE_NAME))) {
            for (IceCreamOrder iceCreamOrder : ordersList){
                outputFile.println(iceCreamOrder.toDataString());
            } 
        } catch (IOException ex){
            System.out.println("Error writing data back to file.");
        }
    }
    
}
