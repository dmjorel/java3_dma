/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalflights;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author Diego Marco
 */
public class Flight {
    
    public Flight(int id, Date onDate, String fromCode, String toCode, Type type, int passengers) {
        this.id = id;
        this.onDate = onDate;
        this.fromCode = fromCode;
        this.toCode = toCode;
        this.type = type;
        this.passengers = passengers;
    }
    
    int id;
    Date onDate;
    String fromCode, toCode;
    Type  type;
    enum Type { Domestic, International, Private };
    int passengers;

    @Override
    public String toString() {
        return "Flight id = " + id + ", on date = " + onDate + ", from " + fromCode + " to " + toCode + ", type = " + type + ", and with " + passengers + " passengers.";
    }

    static SimpleDateFormat dateFormatDisplay = new SimpleDateFormat("yyyy-MM-dd");

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOnDate() {
        return onDate;
    }

    public void setOnDate(Date onDate) {
        this.onDate = onDate;
    }

    public String getFromCode() {
        return fromCode;
    }

    public void setFromCode(String fromCode) {
        this.fromCode = fromCode;
    }

    public String getToCode() {
        return toCode;
    }

    public void setToCode(String toCode) {
        this.toCode = toCode;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public static SimpleDateFormat getDateFormatDisplay() {
        return dateFormatDisplay;
    }

    public static void setDateFormatDisplay(SimpleDateFormat dateFormatDisplay) {
        Flight.dateFormatDisplay = dateFormatDisplay;
    }
    
    
}
