/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalflights;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Diego Marco
 */
public class Database {
    
    
    private static final String dbURL = "jdbc:mysql://localhost:3306/finaldb";
    private static final String username = "root";
    private static final String password = "root2021";
    
    private Connection conn;
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }
    
    public ArrayList<Flight> getAllFlights() throws SQLException {
        ArrayList<Flight> list = new ArrayList<>();
        String sql = "SELECT * FROM flights";
        PreparedStatement statement = conn.prepareStatement(sql);
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                Date onDate = result.getDate("onDay");
                String fromCode = result.getString("fromCode");
                String toCode = result.getString("toCode");
                String typeStr = result.getString("type");
                Flight.Type type = Flight.Type.valueOf(typeStr);
                Integer passengers = result.getInt("passengers");
                Flight flight = new Flight(id, onDate, fromCode, toCode, type, passengers);
                list.add(flight);
            }
        }
        return list;
    }
    
    public int countAllFlights() throws SQLException {
        int count = 0;
        String sql = "SELECT id FROM flights";
        PreparedStatement statement = conn.prepareStatement(sql);
        try (ResultSet result = statement.executeQuery(sql)) {
            while(result.next()){
                count++;
            }
        }
        return count;
    }
    
    int addFlight(Flight flight) throws SQLException {
        try {
            PreparedStatement stmtInsert = conn
                .prepareStatement("INSERT INTO flights VALUES (NULL, ?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
            stmtInsert.setDate(1, new java.sql.Date(flight.getOnDate().getTime()));
            stmtInsert.setString(2, flight.getFromCode());
            stmtInsert.setString(3, flight.getToCode());
            stmtInsert.setString(4, flight.getType() + "");
            stmtInsert.setInt(5, flight.getPassengers());
            stmtInsert.executeUpdate();
        } catch (SQLException ex){
            throw new SQLException("Error adding new flight.");
        }
        return 0;
        
    }
    
   int updateFlight(Flight flight) throws SQLException {
        String sql = "UPDATE flights SET onDay=?, fromCode=?, toCode=?, type=?, passengers=? WHERE id=?";
        PreparedStatement stmtUpdate = conn.prepareStatement(sql);
        stmtUpdate.setDate(1, new java.sql.Date(flight.getOnDate().getTime()));
        stmtUpdate.setString(2, flight.getFromCode());
        stmtUpdate.setString(3, flight.getToCode());
        stmtUpdate.setString(4, flight.getType() + "");
        stmtUpdate.setInt(5, flight.getPassengers());
        stmtUpdate.setInt(6, flight.getId());
        stmtUpdate.executeUpdate();
        System.out.println("Flight updated: ID= " + flight.id);
        
        return 0;
    }
    
    // fetch one record, including the BLOB
    Flight getFlightById(int id) throws SQLException {
        PreparedStatement stmtSelect = conn.prepareStatement("SELECT * FROM flights WHERE id=?");
        stmtSelect.setInt(1, id);
        try (ResultSet resultSet = stmtSelect.executeQuery()) {
            if (resultSet.next()) {
                Date onDate = resultSet.getDate("onDay");
                String fromCode = resultSet.getString("fromCode");
                String toCode = resultSet.getString("toCode");
                String typeStr = resultSet.getString("type");
                Flight.Type type = Flight.Type.valueOf(typeStr);
                int passengers = resultSet.getInt("passengers");
                
                return new Flight(id, onDate, fromCode, toCode, type, passengers);
            } else {
                throw new SQLException("Record not found");
            }
        }
    }
    
}
