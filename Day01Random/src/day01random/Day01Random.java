/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01random;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Diego Marco
 */
public class Day01Random {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        try {
            
            Scanner input = new Scanner(System.in);
        
            System.out.println("How many random numbers user wants?");
            int number = input.nextInt();

            System.out.println("Min: ");
            int min = input.nextInt();

            System.out.println("Max: ");
            int max = input.nextInt();

            if (number < 0 && min < 0 && max < 0){
                System.out.println("Error: Count must not be negative.");
            } else if (max < min){
                System.out.println("Error, max must be larger than min.");
            }

            String result = "";
            for (int i = 0; i < number; i++){
                int rand = (int) (Math.random()*(max-min+1) + min);
                System.out.printf("%s%d", (i == 0? "": ", "), rand);
                result += rand + ", ";
            }
            System.out.println("");
            System.out.println("Result: " + result);
            


        } catch (InputMismatchException ex) {
            System.out.println("Error: value must be integer.");
        }
    }
    
}
