/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Diego Marco
 */
public class Database {
    
    private static final String dbURL = "jdbc:mysql://localhost:3306/day11library";
    private static final String username = "root";
    private static final String password = "root2021";
    
    private Connection conn;
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }
    
    public ArrayList<Book> getAllBooks() throws SQLException {
        ArrayList<Book> list = new ArrayList<>();
        String sql = "SELECT Id,ISBN,titleAndAuthor FROM students";
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a good practice to use try-with-resources for ResultSet so it is freed up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("Id");
                String ISBN = result.getString("ISBN");
                String titleAndAuthor = result.getString("titleAndAuthor");
                // we do not fetch the image here, on purpose
                // System.out.printf("%d: %s is %d y/o\n", id, name, age);
                list.add(new Book(id, ISBN, titleAndAuthor, null));
            }
        }
        return list;
    }
    
    // Insert
    public void addBook(Book book) throws SQLException {
        String sql = "INSERT INTO library VALUES (NULL, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, book.ISBN);
        statement.setString(2, book.titleAndAuthor);
        statement.setBytes(3, book.coverImage);
        // TODO: handle image
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted");
    }
    
}
