/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11library;

import java.util.Comparator;

/**
 *
 * @author Diego Marco
 */
public class Book {

    public Book(int id, String ISBN, String titleAndAuthor, byte[] coverImage) {
        this.Id = id;
        this.ISBN = ISBN;
        this.titleAndAuthor = titleAndAuthor;
        this.coverImage = coverImage;
    }
    
    int Id;
    String ISBN; // 13 or 10, with hyphens, use varchar(20), require numbers and hyphens only, last character may be X (uppercase)
    String titleAndAuthor; // 2-200 characters, any
    byte[] coverImage; // BLOB, may be null

    @Override
    public String toString() {
        return "Book{" + "id=" + Id + ", isbn=" + ISBN + ", titleAndAuthor=" + titleAndAuthor + "}";
    }
    
    // if int, this method only works ...
    public static final Comparator<Book> compareById = (Book o1, Book o2) -> o1.Id - o2.Id;
    
    public static final Comparator<Book> compareByISBN = new Comparator<Book>(){
        @Override
        public int compare(Book arg0, Book arg1) {
            return arg0.ISBN.compareTo(arg1.ISBN);
        }
    };
    
    public static final Comparator<Book> compareByTitleAndAuthor = new Comparator<Book>(){
        @Override
        public int compare(Book arg0, Book arg1) {
            return arg0.titleAndAuthor.compareTo(arg1.titleAndAuthor);
        }
    };
    
}
