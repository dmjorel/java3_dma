// ********** Stack practice
package day02practice;

import java.util.Stack;

public class Day02Practice {


    public static void main(String[] args) {
        Stack<String> Tower = new Stack<String>();
        Tower.add("Red");
        Tower.add("Blue");
        Tower.add("Green");
        
        System.out.println(Tower);
        System.out.println(Tower.peek());
    }
    
}
