
package day01homework_v2;

import java.util.Scanner;

//

public class Day01Homework_v2 {
    
    public static double addtwo(double val1, double val2){
        double answer = val1 + val2;
        return answer;
    }
    
    public static double cel2fah(double val3){
        double answer = (val3*(9.0/5) + 32);
        return answer;
    }
    
    public static double fah2cel(double val4){
        double answer = ((val4 - 32) * (5.0/9));
        return answer;
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter value 1: ");
        double value1 = input.nextDouble();
        System.out.println("Enter value 2: ");
        double value2 = input.nextDouble();
        addtwo(value1,value2);
        System.out.println("Answer: " + addtwo(value1,value2));
        
        System.out.println("Enter Celcius value: ");
        double value3 = input.nextDouble();
        cel2fah(value3);
        System.out.println("Celcius to Fahrenheit: " + cel2fah(value3));
        
        System.out.println("Enter Fahrenheit value: ");
        double value4 = input.nextDouble();
        fah2cel(value4);
        System.out.println("Fahrenheit to Celcius: " + fah2cel(value4));
    }
    
    
}
