
package day04appointments;

import java.util.Date;

enum Reason { 
    CHECKUP, REFERRAL, TESTS, FOLLOWUP, UNWELL; 
}

public class AppItem {
//    Appointment(Date date, int durMin, String name, HashSet<Reason> reasonList) { ... }
//    Appointment(String dataLine) { ... }

    Date date; // year, month, day, hours and minutes, appointment can only be made for tomorrow or a later date
    int durationMinutes; // setters verify only 20,40,60 is allowed,
    String name; // regex to allow only uppercase, lowercase, digits, space, -,.()'"
    String reason; // must not be empty when set (must have at least one reson to visit)

    public AppItem(Date date, int durationMinutes, String name, String reason) {
        setDate(date);
        setDurationMinutes(durationMinutes);
        setName(name);
        setReason(reason);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) { // year, month, day, hours and minutes, appointment can only be made for tomorrow or a later date
        this.date = date;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) throws DataInvalidException { // setters verify only 20,40,60 is allowed,
        if (durationMinutes != 20 || durationMinutes != 40 || durationMinutes != 60){
            throw new DataInvalidException("Error: duration can only be 20, 40 or 60 minutes.");
        }
        this.durationMinutes = durationMinutes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) { // regex to allow only uppercase, lowercase, digits, space, -,.()'"
        if (name.matches("[^a-zA-Z0-9.()'_-]+$")) {
            throw new DataInvalidException("Task must be 2-50 character long and not contain ;|` characters");
        }
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) { // must not be empty when set (must have at least one reson to visit)
        switch (reason){
            case 
        }
        this.reason = reason;
    }
    

    toString() { ... }
    // example ToString output:
    // Appointment on 2021-04-22 at 14:20 for 40 minutes for Jerry M. Boe-Eing, reasons: UNWELL,TESTS

    toDataString() { ... }

    // add anonymous classes here for sorting, using Comparator classes, or use lambda if you like
    

}
