
package day04appointments;

import java.text.SimpleDateFormat;

public class Globals {
    // TODO: use toLocalizedPattern()
    static final SimpleDateFormat dateFormatScreen = new SimpleDateFormat("yyyy/MM/dd 'at' hh:mm");
    
    static { // static initializer
        dateFormatScreen.setLenient(false);
    }
}
