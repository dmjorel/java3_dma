
package day04appointments;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Day04Appointments {

    static ArrayList<AppItem> AppList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    
    
    static int inputInt(){
        while(true){
            try{
                int result = input.nextInt();
                input.nextLine();
                return result;
            } catch (InputMismatchException ex){
                System.out.println("Invalid input, enter an integer. Try again.");
                input.nextLine();
            }
        }
    }
    
    static int getMenuOption(){
        System.out.println("Please choose from the following options: "
                + "1. Make An Appointment."
                + "2. List appointments by Date."
                + "3. List appointments by Name."
                + "4. List appointments by their first Reason."
                + "0. Exit." );
        int choice = inputInt();
        return choice;  
    }
    
    public static void main(String[] args) {
        
        while (true) {
            int option = getMenuOption();
            switch (option){
                case 1:
                    addAppoint();
                    break;
                case 2:
                    sortByDate();
                    break;
                case 3:
                    sortByName();
                    break;
                case 4:
                    sortByReason();
                    break;
                case 0:
                    System.out.println("Exiting.");
                    return;
                default:
                    System.out.println("Invalid choice, try again");
            }
        }
    }
    
    
    private static void addAppoint(){
        try{
            System.out.println("Adding an Appointment.");
            // appointment date
            System.out.println("Enter Appointment date: ");
            String dateStr = input.nextLine();
            Date date = Globals.dateFormatScreen.parse(dateStr);

            // appointment duration - only be 20, 40 or 60 minutes
            System.out.println("Enter Appointment duration: ");
            int duration = input.nextInt();
            
            // appointment name of patient
            System.out.println("Enter name of patient: ");
            String name = input.nextLine();
            
            // appointment reason
            System.out.println("Enter appointment reason: ");
            String reason = input.nextLine();
            
            // adding all the info to appointment
            AppItem appointItem = new AppItem(date, duration, name, reason);
            AppList.add(appointItem);
        } catch (ParseException | NumberFormatException ex) {
            System.out.println("Error parsing: " + ex.getMessage());
        } 
    }
    // collection.sort
    private static void sortByDate(){
        AppList.sort(new Comparator<AppItem>(){
            @Override
            public int compare(AppItem arg0, AppItem arg1) {
                int comparison = 0;
                comparison = arg0.getDate().compareTo(arg1.getDate());
                if (comparison == 0){
                    comparison = arg0.getName().compareTo(arg1.getName());
                } 
                return comparison;
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
        });
        for(AppItem eachAppItem : AppList){
            System.out.println(eachAppItem);
        }
    }

    private static void sortByName(){
        AppList.sort(new Comparator<AppItem>(){
            @Override
            public int compare(AppItem arg0, AppItem arg1) {
                int comparison = 0;
                comparison = arg0.getName().compareTo(arg1.getName());
                if (comparison == 0){
                    comparison = arg0.getReason().compareTo(arg1.getReason());
                } 
                return comparison;
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
        });
        for(AppItem eachAppItem : AppList){
            System.out.println(eachAppItem);
        }
    }
    
    private static void sortByReason(){
        AppList.sort(new Comparator<AppItem>(){
            @Override
            public int compare(AppItem arg0, AppItem arg1) {
                int comparison = 0;
                comparison = arg0.getReason().compareTo(arg1.getReason());
                if (comparison == 0){
                    comparison = arg0.getDate().compareTo(arg1.getDate());
                } 
                return comparison;
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
        });
        for(AppItem eachAppItem : AppList){
            System.out.println(eachAppItem);
        }
    }
    
    
    
    

}



/*

        catch (DataInvalidException ex) {
            System.out.println("Error: " + ex.getMessage());
        } 

            // appointment time - minutes must only be :20, :40, :00
            System.out.println("Enter Appointment time: ");
            String timeStr = input.nextLine();
            LocalTime localTime = LocalTime.parse(timeStr, DateTimeFormatter.ofPattern("HH:mm"));
            int hour = localTime.get(ChronoField.CLOCK_HOUR_OF_DAY);
            int minute = localTime.get(ChronoField.MINUTE_OF_HOUR);

*/

