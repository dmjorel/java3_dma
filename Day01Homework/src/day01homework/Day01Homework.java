
package day01homework;

import java.io.File;
import java.util.Scanner;


public class Day01Homework {
    
    Scanner input = new Scanner(System.in);
    
    static final String DATA_FILE_NAME01 = "addtwo.txt";
    static final String DATA_FILE_NAME02 = "cel2fah.txt";
    static final String DATA_FILE_NAME03 = "fah2cel.txt";
    
    private static void addTwo(){
        try (Scanner fileInput = new Scanner(new File(DATA_FILE_NAME01))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                try {
                    if (line == "read:Enter value 1"){
                        System.out.println("Enter value 1");
                        long val1 = input.nextLong();
                    } else if (line == "read:Enter value 2"){
                        
                    }
                } catch {
                    
                }
            }
        } catch (NumberFormatException ex){
            System.out.println("");
        }
    }
    
    private static void cel2fah(){
        try (Scanner fileInput = new Scanner(new File(DATA_FILE_NAME02))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                
            }
        } catch (NumberFormatException ex){
            System.out.println("");
        }
    }
    
    private static void fah2cel(){
        try (Scanner fileInput = new Scanner(new File(DATA_FILE_NAME03))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                
            }
        } catch (NumberFormatException ex){
            System.out.println("");
        }
    }
    
    
    
    public static void main(String[] args) {
        addTwo();
        cel2fah();
        fah2cel();
    }
    
}
