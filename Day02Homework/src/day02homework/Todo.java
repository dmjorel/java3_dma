
package day02homework;

import java.security.InvalidParameterException;
import java.util.Date;

public class Todo {
    String task; // 2-50 characters long, must NOT contain a semicolon or | or ` (reverse single quote) characters
    Date dueDate; // Date between year 1900 and 2100
    int hoursOfWork; // 0 or greater number

    public Todo(String task, Date dueDate, int hoursOfWork) {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
    }
    
    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        if (task.length() < 2 || task.length() > 50 || task.contains(";") || task.contains("`")){
            throw new InvalidParameterException("Error: 2-50 characters long, must NOT contain a semicolon or | or ` (reverse single quote) characters.");
        }
        this.task = task;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        if (dueDate.getYears() < 1900 || dueDate.getYears() > 2100){
            throw new InvalidParameterException("Error: Date between year 1900 and 2100.");
        }
        this.dueDate = dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setHoursOfWork(int hoursOfWork) {
        if (hoursOfWork < 0){
            throw new InvalidParameterException("Error: 0 or greater number");
        }
        this.hoursOfWork = hoursOfWork;
    }

    @Override
    public String toString() {
        return "Todo{" + "task=" + task + ", dueDate=" + dueDate + ", hoursOfWork=" + hoursOfWork + '}';
    }
    
    
}
